'use strict';
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var router = express.Router();

app.use(express.static('public'));

app.get('*', function(req, res) {
    res.status(404).sendFile(__dirname + '/public/404.html');
});

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/public/index.html');
});

app.listen(5000);
