var gulp        = require('gulp')
var stylus      = require('gulp-stylus')
var plumber     = require('gulp-plumber')
var jade        = require('gulp-jade')
var nodemon     = require('gulp-nodemon')
var browserSync = require('browser-sync').create();

gulp.task('stylus', function(){
    return gulp.src('src/stylus/main.styl')
        .pipe(plumber())
        .pipe(stylus())
        .pipe(plumber.stop())
        .pipe(gulp.dest('./public/css'))
})

gulp.task('jade', function() {
  gulp.src('./src/jade/**/*.jade')
    .pipe(plumber())
    .pipe(jade({
        pretty: true
    }))
    .pipe(plumber.stop())
    .pipe(gulp.dest('./public'))
})

gulp.task('watch', function(){
    gulp.watch('src/stylus/**/*.styl', ['stylus']);
    gulp.watch('src/jade/**/*.jade', ['jade']);
})

gulp.task('nodemon', function (cb) {
    var started = false;    
    return nodemon({
        script: 'app.js'
    }).on('start', function () {
        if (!started) {
            cb();
            started = true; 
        } 
    });
});

gulp.task('server',['nodemon'], function () {
    browserSync.init(null, {
        proxy: "http://localhost:5000",
        browser: "google chrome",
        port: 7000,
    });
})

gulp.task('default', ['jade', 'stylus'])